var canvas, ctx;
var draw;
var initX, initY;

var mode = "pencil";

function init(){
	canvas = document.getElementById("myCanvas");
	ctx = canvas.getContext('2d');
}


function clearCanvas(){
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	// location.reload();	// another way
}

function mo(){
	if(mode == "pencil") 
		document.getElementById("myCanvas").style.cursor = "url(pencil.png), pointer";
	else if(mode == "eraser") 
		document.getElementById("myCanvas").style.cursor = "url(eraser.png), pointer";
	else if(mode == "line") 
		document.getElementById("myCanvas").style.cursor = "url(line.png), pointer";
	else if(mode == "triangle") 
		document.getElementById("myCanvas").style.cursor = "url(triangle.png), pointer";
	else if(mode == "circle") 
		document.getElementById("myCanvas").style.cursor = "url(circle.png), pointer";
	else if(mode == "rect") 
		document.getElementById("myCanvas").style.cursor = "url(rect.png), pointer";
	else if(mode == "text") 
		document.getElementById("myCanvas").style.cursor = "url(text.png), pointer";
}

function md(){	//媽蛋
	ctx.beginPath();
	ctx.moveTo(event.offsetX, event.offsetY);
	draw = true;
	initX = event.offsetX;
	initY = event.offsetY;
	if(mode == "text"){
		var x = document.getElementById("myText").value;

		ctx.font = "30px Arial";
		ctx.fillText(x, initX, initY);
	}
}

function mv(){
	
	var thickness =  document.getElementById("numSelector").value / 5;
	var brushColor = document.getElementById("colorSelector").value;
	ctx.globalCompositeOperation = "source-over";
	if(draw){
		ctx.lineWidth = thickness;
		ctx.strokeStyle = brushColor;
		ctx.fillStyle = brushColor;

		if(mode == "pencil"){
			ctx.lineTo(event.offsetX, event.offsetY);
			ctx.stroke();
		}
		else if(mode == "eraser"){
			ctx.globalCompositeOperation = "destination-out";
			ctx.lineTo(event.offsetX, event.offsetY);
			ctx.stroke();
		}
	}
}

function mup(){
	if(draw){

		ctx.lineWidth = document.getElementById("numSelector").value / 10;
		ctx.strokeStyle = document.getElementById("colorSelector").value;
		if(mode == "rect"){
			ctx.fillRect(initX, initY, event.offsetX - initX, event.offsetY - initY);
		}
		else if(mode == "line"){
			ctx.lineTo(event.offsetX, event.offsetY);
			ctx.stroke();
		}
		else if(mode == "circle"){
			var Rx = Math.abs(event.offsetX - initX);
			var Ry = Math.abs(event.offsetY - initY);
			var circleR = Math.sqrt(Rx*Rx + Ry*Ry);
			ctx.arc(initX, initY, circleR, 0, 2*Math.PI);
			ctx.stroke();
		}
		else if(mode == "triangle"){
			ctx.lineTo(event.offsetX,initY);
			ctx.lineTo(0.5*(event.offsetX + initX),event.offsetY);
			ctx.closePath();
			ctx.stroke();
			ctx.fill();
		}
	}
	draw = false;
	ctx.closePath();
	if(mode == "circle")ctx.fill();
}

function myFunc(n){	// n means "which mode", line for example
	document.querySelector('#btn_' + mode).classList.remove("active");
	mode =  n;
	document.querySelector('#btn_' + n).classList.add("active");
}
